(function () {
    'use strict';
    //创建了一个模块和控制器并在控制器中调用了alarmsctrl这个警报控制的函数
    angular.module('helloCoreApi').controller('AlarmsCtrl', [
        AlarmsCtrl
    ]);

    function AlarmsCtrl() {
        //警报的类别
        this.severities = [
            {name: '危险', value: '危险', cls: 'btn-danger'},
            {name: '主要', value: '主要', cls: 'btn-warning'},
            {name: '次要', value: '次要', cls: 'btn-primary'},
            {name: '警告', value: '警告', cls: 'btn-info'}
        ];

        this.onClick = function (filter, severity) {
            //在点击的函数内用过滤器的类别和警报级别判断，如果相等就不用再过滤了，直接显示出警报级别。
            //不相等把警报级别赋值给过滤器再显示
            if (filter.severity === severity.value) {
                filter.severity = undefined;
            } else {
                filter.severity = severity.value;
            }
        };
        //判断警报是不是活动的函数，并且把警报的值赋值给过滤器
        this.isActive = function (filter, severity) {
            return filter.severity === severity.value;
        };
    }
})();
