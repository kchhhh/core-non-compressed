(function () {
    'use strict';

    var app = angular.module('helloCoreApi');
    app.controller('mapCtrl', [
        '$scope',
        '$http',
        '$q',
        'c8yCepModule',
        'c8yMeasurements',
        'c8yBase',
        'info',
        '$rootScope',
        '$window',
        '$routeParams',
        'c8yInventory',
        '$cacheFactory',
        'c8yDevices',
        'c8yGroupTypesConfig',
        'c8yGroups',
        'c8yEvents',
        MapCtrl
    ]);
    app.directive('egSection', [
        egSection
    ]);

    function MapCtrl($scope, $http, $q, c8yCepModule, info, c8yBase, $rootScope, $window, $routeParams, c8yInventory, c8yMeasurements, $cacheFactory, c8yDevices, c8yGroupTypesConfig, c8yGroups, c8yEvents) {
        c8yEvents.list(
            angular.extend(c8yBase.timeOrderFilter(), {})
        ).then(function (events) {
            $scope.events = events;
            $scope.arr = [];
            angular.forEach($scope.events, function (data) {
                $scope.arr.push(data.c8y_Position.lng+','+data.c8y_Position.lat);
            });
            var map = new AMap.Map('container', {
                resizeEnable: true,
                zoom: 11,
                center: [116.468711, 40.022691]
            });
            map.setMapStyle("light");
            for (var i = 0 ; i < $scope.arr.length ; i ++ ){
                var marker = new AMap.Marker({
                    map:map,
                    // icon: $scope.icon,//24px*24px
                    position: $scope.arr[i].split(','),
                    offset: new AMap.Pixel(0, 0)
                });
            }
        });
    }

    function egSection() {
        return {
            restrict: 'AE',
            templateUrl: 'views/sections/map.html',
            controller: 'mapCtrl',
            controllerAs: 'map'
        };
    }
})();
