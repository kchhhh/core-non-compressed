(function () {
    'use strict';
    //创建一个app的模块
    var app = angular.module('helloCoreApi', [
        //引入依赖的一些模块
        'c8y.sdk',
        'c8y.ui',
        'ngRoute',
        'ui.bootstrap'
    ]);
    //配置路由路径
    app.config([
        '$routeProvider',
        //调用路径的函数
        configRoutes
    ]);
    //配置cumulocity
    app.config([
        'c8yCumulocityProvider',
        configCumulocity
    ]);
//路径的函数
    function configRoutes($routeProvider) {
        $routeProvider
            .when('/login', {
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl',
                controllerAs: 'login'
            })
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })
            .when('/:section', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            });
    }

    function configCumulocity(c8yCumulocityProvider) {
        // 设置应用程序建
        c8yCumulocityProvider.setAppKey('core-application-key');
        //设置基本的URL
        c8yCumulocityProvider.setBaseUrl('https://demos.cumulocity.com/');
    }
})();
